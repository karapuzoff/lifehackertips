<?php
/**
 * Template part for displaying header page title
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blogar
 */

// Get Value
$axil_options = Helper::axil_get_options();

if ($axil_options['axil_enable_blog_title'] !== 'no')
{

  $category_image_id 	= get_term_meta( @get_queried_object()->term_id, 'blogar_category_background_image', true );
  $background_img     = wp_get_attachment_image_url( $category_image_id, 'large' );
?>
<!-- Start Breadcrumb Area  -->
<style>
.bg-category-image::before {
  background-image: url('<?= $background_img ?>');
}
</style>
<div class="axil-breadcrumb-area breadcrumb-style-1 bg-color-grey bg-category-image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner">
                    <?php  if ( !is_home() && "hide" !== $axil_options['axil_enable_single_post_breadcrumb_wrap']) {
                        axil_breadcrumbs();
                    } ?>
                    <?php
                    if($axil_options['axil_enable_blog_title'] !== 'no'){
                        if ( is_archive() ): ?>
                            <h1 class="page-title"><?php the_archive_title(); ?></h1>
                            <p><?php the_archive_description(); ?></p>
                        <?php elseif( is_search() ): ?>
                            <h1 class="page-title"><?php esc_html_e( 'Search results for: ', 'blogar' ) ?><?php echo get_search_query(); ?></h1>
                        <?php else: ?>
                            <h1 class="page-title">
                                <?php  if ( isset( $axil_options['axil_blog_text'] ) && !empty( $axil_options['axil_blog_text'] ) ){
                                    echo esc_html( $axil_options['axil_blog_text'] );
                                } else {
                                    echo esc_html__('Blog', 'blogar');
                                }  ?>
                            </h1>
                        <?php endif;
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb Area  -->
<?php }
