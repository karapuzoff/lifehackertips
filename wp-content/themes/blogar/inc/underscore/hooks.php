<?php
/*
 * Версия сайта
 */
class SiteVersion
{
  const MAJOR = 1;
  const MINOR = 0;
  const PATCH = 1;

  public static function get()
  {
    $commitHash = trim(exec('git log --pretty="%h" -n1 HEAD'));

    $commitDate = new \DateTime(trim(exec('git log -n1 --pretty=%ci HEAD')));
    $commitDate->setTimezone(new \DateTimeZone('UTC'));

    return sprintf('v%s.%s.%s-dev.%s (%s)', self::MAJOR, self::MINOR, self::PATCH, $commitHash, $commitDate->format('d.m.Y H:i:s'));
  }
}

## Удаляет "Рубрика: ", "Метка: " и название сайта ("спасибо" SEO-плагину) из заголовка страниц
add_filter ('get_the_archive_title', 'page_title_normalize');
function page_title_normalize( $title )
{
 $title = preg_replace('~^[^:]+: ~', '', $title );
 return $title;
}

## Удаляет все ссылки из текста поста
add_filter ('the_content', 'delete_ext_links');
function delete_ext_links ($content)
{
 $domainsArray = [
   'lifehacker.ru',
   'lenta.ru'
 ];
 $domainsString = implode("|", $domainsArray);
 $domainsString = str_replace(".", "\.", $domainsString);/**/

 $pattern = '~|<a[^<]+(' . $domainsString . ')[^>]+>([^<]+)<\/a>|~';

 $content = preg_replace($pattern, '$2', $content);

 return $content;
}
/**/

## Register oEmbed Instagram
function instagram_oembed_provider()
{
 wp_oembed_add_provider( '#https?://(www.)?instagram.com/p/.*#i', 'http://api.instagram.com/oembed', true );
 wp_oembed_add_provider( '#https?://(www.)?instagr.am/p/.*#i', 'http://api.instagram.com/oembed', true );
}
add_action( 'init', 'instagram_oembed_provider' );

add_filter('embed_handler_html', 'custom_instagram_settings');
add_filter('embed_oembed_html', 'custom_instagram_settings');
function custom_instagram_settings($code)
{
 if (strpos($code, 'instagr.am') !== false || strpos($code, 'instagram.com') !== false)
 { // if instagram embed
   $return = preg_replace("@data-instgrm-captioned@", "", $code); // remove caption class
   return $return;
 }
 return $code;
}

## Контроль версий для style.css
add_action('wp_enqueue_scripts', 'theme_style_version');
function theme_style_version()
{
 wp_enqueue_style('style', get_stylesheet_directory_uri() .'/style.css', array(), '1.1.3' );
}

## Post View Count
/**
* To display number of posts.
*
* @param $postID current post/page id
*
* @return string
*/
function get_post_view( $postID ) {
 $count_key = 'post_views_count';
 $count     = get_post_meta( $postID, $count_key, true );
 if ( $count == '' ) {
   delete_post_meta( $postID, $count_key );
   add_post_meta( $postID, $count_key, '0' );
 }

 return $count;
}

/**
* To count number of views and store in database.
*
* @param $postID currently viewed post/page
*/
function set_post_view( $postID ) {
 $count_key = 'post_views_count';
 $count     = (int) get_post_meta( $postID, $count_key, true );
 if ( $count < 1 ) {
   delete_post_meta( $postID, $count_key );
   add_post_meta( $postID, $count_key, '1' );
 } else {
   $count++;
   update_post_meta( $postID, $count_key, (string) $count );
 }
}

/**
* Add a new column in the wp-admin posts list
*
* @param $defaults
*
* @return mixed
*/
function posts_column_views( $defaults ) {
 $defaults['post_views'] = __( 'Просмотров' );

 return $defaults;
}

/**
* Display the number of views for each posts
*
* @param $column_name
* @param $id
*
* @return void simply echo out the number of views
*/
function posts_custom_column_views( $column_name, $id ) {
 if ( $column_name === 'post_views' ) {
   echo get_post_view( get_the_ID() );
 }
}

/**
* function to reset the post view counts.
*/
function reset_postview_counts() {
 $count_key = 'post_views_count';
 $args = array(
   'posts_per_page' => -1,
   'meta_key' => $count_key,
   'post_type' => 'post',
   'post_status' => 'publish',
   'suppress_filters' => true,
   'no_found_rows' => true,
   'fields' => 'ids',
 );
 $postslist = get_posts( $args );
 foreach ( $postslist as $singlepostid ) {
   // reset all the post's count to 0
   update_post_meta( $singlepostid, $count_key, '1' );
 }
}

add_filter( 'manage_posts_columns', 'posts_column_views' );
add_action( 'manage_posts_custom_column', 'posts_custom_column_views', 5, 2 );

/**
 * function to show social sharing buttons.
 */
function puz_sharing_icons()
{
?>
<style>
/** Fix отображения Share-блоков в мобильном разрешении **/
@media (max-width: 580px) {
	.ya-share2 {
		margin-top: 20px;
	}
}
</style>
<script src="https://yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-curtain data-shape="round" data-color-scheme="whiteblack" data-services="vkontakte,facebook,odnoklassniki,telegram,twitter"></div>
<?php
}
