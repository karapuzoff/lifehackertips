<?php
add_action('wp_enqueue_scripts', 'blogar_child_enqueue_styles');
function blogar_child_enqueue_styles() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('blogar-child-style', get_stylesheet_uri() );
}
