<?php
/**
 * Plugin Name: Blogar Core
 * Plugin URI: #
 * Description: Blogar core.
 * Version: 1.0.0
 * Author: Axilthemes
 * Author URI: http://axilthemes.com/
 * Text Domain: blogar
*/

/**
* Define
*/
define( 'BLOGAR_ADDONS_VERSION', '1.0.0' );
define( 'BLOGAR_ADDONS_URL', plugins_url( '/', __FILE__ ) );
define( 'BLOGAR_ADDONS_DIR', dirname( __FILE__ ) );
define( 'BLOGAR_ADDONS_PATH', plugin_dir_path( __FILE__ ) );
define( 'BLOGAR_ELEMENTS_PATH', BLOGAR_ADDONS_DIR . '/include/elementor');


include_once(BLOGAR_ADDONS_DIR . '/include/blogar-after-activation.php');
register_activation_hook(__FILE__, 'blogar_activate_core');

/**
 * Include all files
 */
include_once(BLOGAR_ADDONS_DIR. '/include/custom-post.php');
include_once(BLOGAR_ADDONS_DIR. '/include/social-share.php');
include_once(BLOGAR_ADDONS_DIR. '/include/widgets/custom-widget-register.php');
include_once(BLOGAR_ADDONS_DIR. '/include/common-functions.php');
include_once(BLOGAR_ADDONS_DIR. '/include/allow-svg.php');
include_once(BLOGAR_ADDONS_DIR. '/include/ajax_requests.php');
include_once(BLOGAR_ADDONS_DIR. '/include/sidebar-generator.php');

/**
 * Load Custom Addonss
 */
if ( in_array('elementor/elementor.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
    function blogarLoadCustomElements(){
        if ( is_dir( BLOGAR_ELEMENTS_PATH ) && $blogar_cc_dirhandle = opendir( BLOGAR_ELEMENTS_PATH )) {
            while ( $blogar_cc_file = readdir( $blogar_cc_dirhandle )) {
                if ( !in_array( $blogar_cc_file, array('.', '..') )) {
                    $blogar_cc_file_contents = file_get_contents( BLOGAR_ELEMENTS_PATH . '/' . $blogar_cc_file );
                    $blogar_cc_php_file_tokens = token_get_all( $blogar_cc_file_contents );
                    require_once( BLOGAR_ELEMENTS_PATH . '/' . $blogar_cc_file );
                }
            }
        }
    }
    add_action('elementor/widgets/widgets_registered','blogarLoadCustomElements');
}

/**
 * Add Category
 */
if ( in_array('elementor/elementor.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
    function blogar_elementor_category ( $elements_manager ) {
        $elements_manager->add_category(
            'blogar',
            array(
                'title' => esc_html__( 'Blogar Addons', 'blogar' ),
                'icon'  => 'eicon-banner',
            )
        );
    }
    add_action( 'elementor/elements/categories_registered', 'blogar_elementor_category' );
}



/**
 * Escapeing
 */
if ( !function_exists('blogar_escapeing')) {
    function blogar_escapeing($html){
        return $html;
    }
}

/**
 * Load module's scripts and styles if any module is active.
 */
function blogar_element_enqueue(){
    // wp_enqueue_style('essential_addons_elementor-css',BLOGAR_ADDONS_URL.'assets/css/blogar.css');
    wp_enqueue_script('blogar-element-scripts', BLOGAR_ADDONS_URL . 'assets/js/element-scripts.js', array('jquery'),'1.0', true);
}
add_action( 'wp_enqueue_scripts', 'blogar_element_enqueue' );


function blogar_enqueue_editor_scripts(){
    wp_enqueue_style('blogar-element-addons-editor', BLOGAR_ADDONS_URL . 'assets/css/editor.css', null, '1.0');
}
add_action( 'elementor/editor/after_enqueue_scripts', 'blogar_enqueue_editor_scripts');



/**
 * Check elementor version
 *
 * @param string $version
 * @param string $operator
 * @return bool
 */
function axil_is_elementor_version( $operator = '<', $version = '2.6.0' ) {
    return defined( 'ELEMENTOR_VERSION' ) && version_compare( ELEMENTOR_VERSION, $version, $operator );
}



/**
 * Render icon html with backward compatibility
 *
 * @param array $settings
 * @param string $old_icon_id
 * @param string $new_icon_id
 * @param array $attributes
 */
function axil_render_icon( $settings = [], $old_icon_id = 'icon', $new_icon_id = 'selected_icon', $attributes = [] ) {
    // Check if its already migrated
    $migrated = isset( $settings['__fa4_migrated'][ $new_icon_id ] );
    // Check if its a new widget without previously selected icon using the old Icon control
    $is_new = empty( $settings[ $old_icon_id ] );

    $attributes['aria-hidden'] = 'true';

    if ( axil_is_elementor_version( '>=', '2.6.0' ) && ( $is_new || $migrated ) ) {
        \Elementor\Icons_Manager::render_icon( $settings[ $new_icon_id ], $attributes );
    } else {
        if ( empty( $attributes['class'] ) ) {
            $attributes['class'] = $settings[ $old_icon_id ];
        } else {
            if ( is_array( $attributes['class'] ) ) {
                $attributes['class'][] = $settings[ $old_icon_id ];
            } else {
                $attributes['class'] .= ' ' . $settings[ $old_icon_id ];
            }
        }
        printf( '<i %s></i>', \Elementor\Utils::render_html_attributes( $attributes ) );
    }
}

/**
 * Remove Auto p form contact form 7
 */
add_filter( 'wpcf7_autop_or_not', '__return_false' );