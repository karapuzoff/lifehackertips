<?php
/* Переопледеляем конфиги, чтобы для локальной разработки и деплоя использовать разные файлы */

if ( $_SERVER['HTTP_HOST'] == 'localhost' && file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ))
{
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  define( 'WP_LOCAL_DEV', true );
} else {
  include( dirname( __FILE__ ) . '/wp-config-prod.php' );
}
