<?php
header("Content-Type:text/plain");

if ($_SERVER['HTTP_HOST'] != 'lifehackertips.ru')
{
  $robots_rules = [
    'User-agent: *',
    'Disallow: /'
  ];
} else {
  $robots_rules = [
    "User-agent: *\n",
    'Disallow: /wp-admin/',
    "Allow:    /wp-admin/admin-ajax.php\n",
    'Disallow: /cgi-bin',
    'Disallow: /?',
    'Disallow: *?s=',
    'Disallow: *&s=',
    'Disallow: /search',
    'Disallow: /author/',
    'Disallow: */embed',
    'Disallow: */page/',
    'Disallow: */xmlrpc.php',
    'Disallow: *utm*=',
    "Disallow: *openstat=\n",
    'Sitemap: https://'.$_SERVER['HTTP_HOST'].'/sitemap.xml',
    'Sitemap: https://'.$_SERVER['HTTP_HOST'].'/sitemap.rss'
  ];
}

$_str = implode ("\n", $robots_rules);

$_str = trim( $_str );
$_str = preg_replace( '/^[\t ]+(?!#)/mU', '', $_str );
$output = "$_str\n";

echo $output;
